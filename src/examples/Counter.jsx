import React, {useEffect, useState} from "react";
import {Button} from "@material-ui/core";

export default function Counter(props) {

    const [counterValue, setCounterValue] = React.useState(0);
    const [buttonList, setButtonList] = useState([]);


    useEffect(()=>{
        setCounterValue(props.ilkDeger);
        console.log("props: " + JSON.stringify(props));
    }, [props.ilkDeger]);

    const ekle = () => {
        setCounterValue(counterValue + 1);
        buttonEkle();
    };

    const buttonEkle = () => {
        let button = (
            <Button variant="contained" color="default">
                Eklenen Button
            </Button>
        );

      setButtonList([...buttonList, button]);
    };

    return (
        <React.Fragment>
            <div>
                    {buttonList}
            </div>
            <Button variant="contained" color="primary" onClick={ekle}>
                Ekle
            </Button>
        </React.Fragment>
    );

}