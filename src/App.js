import React from 'react';
import logo from './logo.svg';
import './App.css';
import Counter from "./examples/Counter";
import {Board} from "./examples/Board";

function App() {
  return (
    <div className="App">
      <header className="App-header">
          Learn React
          <Counter ilkDeger={0}/>
          <Board />
      </header>
    </div>
  );
}

export default App;
